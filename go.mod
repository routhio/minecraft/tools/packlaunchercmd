module gitlab.com/routhio/minecraft/tools/packlaunchercmd

go 1.14

require (
	github.com/buger/jsonparser v1.1.1 // indirect
	github.com/go-resty/resty/v2 v2.4.0
	github.com/google/uuid v1.2.0
	gitlab.com/routhio/minecraft/tools/packgolib v0.0.0-20210122222448-4a08bafac9b9
	golang.org/x/net v0.0.0-20210119194325-5f4716e94777 // indirect
	golang.org/x/oauth2 v0.0.0-20210113205817-d3ed898aa8a3 // indirect
	golang.org/x/term v0.0.0-20201210144234-2321bbc49cbf
)
