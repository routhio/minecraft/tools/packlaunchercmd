package main

import (
	"fmt"
    "os"
    "path/filepath"

    "github.com/google/uuid"

	"gitlab.com/routhio/minecraft/tools/packgolib"
	"gitlab.com/routhio/minecraft/tools/packgolib/pack"
	"gitlab.com/routhio/minecraft/tools/packgolib/util"

    "gitlab.com/routhio/minecraft/tools/packlaunchercmd/mojangauth"
)

var CACHE_LOCATION = filepath.Join("build", "cache")
var INSTANCES_LOCATION = filepath.Join("build", "instances")

func main() {
	packfile := os.Args[1]

	pack, err := pack.ReadPack(packfile)
	if err != nil {
		panic(err)
	}
	fmt.Printf("NAME: %s\n", pack.Metadata.Name)
	fmt.Printf("TITLE: %s\n", pack.Metadata.Title)
	fmt.Printf("VERSION: %s\n", pack.Metadata.Version)
	fmt.Printf("AUTHORS: %s\n", pack.Metadata.Authors)
	fmt.Printf("\n")
	fmt.Printf("MINECRAFT VERSION: %s\n", pack.Manifest.MinecraftVersion)

	for _, modloader := range pack.Manifest.Modloaders {
		fmt.Printf("MODLOADER: %s %s\n", modloader.Id, modloader.Version)
	}

    // Login to mojang ...

    fmt.Println("\nMinecraft Login ...")
    account := &mojangauth.MojangAccount{
        ClientToken: uuid.NewString(),
    }
    username, password, err := mojangauth.PromptCredentials()
    if err != nil {
        fmt.Printf("PROMPT ERROR: %s\n", err)
    }
    autherr := account.YggAuthenticate(username, password)
    if autherr != nil {
        panic(autherr)
    }

    // Install the pack

	fmt.Printf("\nInstalling ...\n")

	instLocation := filepath.Join(INSTANCES_LOCATION, pack.Metadata.Name)
	downloader := util.NewDownloader(CACHE_LOCATION)

	minecraft, modloader, installerr := pack.Install(instLocation, downloader)
	if installerr != nil {
		panic(installerr)
	}

    // LAUNCH!

	if err := packgolib.Launch(minecraft, modloader, instLocation, account.SelectedProfile.Name, account.SelectedProfile.ID, account.AccessToken); err != nil {
		fmt.Printf("ERROR: %s\n", err)
	}
}

