package mojangauth

import (
    "bufio"
	"fmt"
	"os"
    "strings"
    "syscall"

    "golang.org/x/term"
	"github.com/go-resty/resty/v2"
)

type MojangAccount struct {
    AccessToken string
    ClientToken string
    SelectedProfile MojangProfile
    User MojangUser
}

// Profile holds data about an authenticated user's profile.
type MojangProfile struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Legacy bool   `json:"legacy"`
}

// User holds data about an authenticated user.
type MojangUser struct {
	ID         string           `json:"id"`
	Properties []MojangProperty `json:"properties"`
}

// Property holds data about an authenticated user's property.
type MojangProperty struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

//****************************************************************************** 

func PromptCredentials() (string, string, error) {
    reader := bufio.NewReader(os.Stdin)
    fmt.Print("Mojang username: ")
    username, err := reader.ReadString('\n')
    if err != nil {
        return "", "", err
    }

    fmt.Print("Mojang password: ")
    password, err := term.ReadPassword(int(syscall.Stdin))
    fmt.Println()
    if err != nil {
        return "", "", err
    }

    return strings.TrimSpace(username), string(password), nil
}

const YGG_AUTHSERVER_URL = "https://authserver.mojang.com"

type YggErrorResponse struct {
	Error        string `json:"error"`
	ErrorMessage string `json:"errorMessage"`
	Cause        string `json:"cause"`
	StatusCode   int
}

type YggAuthResponse struct {
	AccessToken       string    `json:"accessToken"`
	ClientToken       string    `json:"clientToken"`
	AvailableProfiles []MojangProfile `json:"availableProfiles"`
	SelectedProfile   MojangProfile   `json:"selectedProfile"`
	User              MojangUser      `json:"user"`
}

func (acct *MojangAccount) YggAuthenticate(username, password string) error {
	client := resty.New()
	resp, err := client.R().
		SetHeader("Content-Type", "application/json").
        SetBody(`{"agent": {"name": "Minecraft", "version": 1}, "username": "` + username + `", "password": "` + password + `", "clientToken": "` + acct.ClientToken + `", "requestUser": true}`).
		SetResult(&YggAuthResponse{}).
		SetError(&YggErrorResponse{}).
		Post(YGG_AUTHSERVER_URL + "/authenticate")
	if err != nil {
		return err
	}
	if resp.StatusCode() != 200 {
		authError := resp.Error().(*YggErrorResponse)
		return fmt.Errorf("Authentication Error: %s", authError.ErrorMessage)
	}

	authResponse := resp.Result().(*YggAuthResponse)
    acct.AccessToken = authResponse.AccessToken
    acct.ClientToken = authResponse.ClientToken
    acct.SelectedProfile = authResponse.SelectedProfile
    acct.User = authResponse.User

    return nil
}
